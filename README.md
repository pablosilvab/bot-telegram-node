# Bot Telegram

El objetivo de este proyecto es implementar un Bot de Telegram. Para esto se utilizó la librería [Telegraf](https://telegraf.js.org/#/)

Busca a ```@takechibot``` en Telegram.

Por ahora invocar [server](https://bot-telegram-node.herokuapp.com) y luego probar.

## Instalación

Instalar dependencias 
```
npm install
```

Ejecutar localmente:
```
npm run dev
```

## Mensajes

* ```clima <nombre-ciudad>```: Retorna la temperatura actual de la ciudad ingresada.

## Comandos


