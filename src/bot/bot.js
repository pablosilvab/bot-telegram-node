require('../config/config')
const lugar = require('../utils/lugar/lugar');
const clima = require('../utils/clima/clima');

const Telegraf = require('telegraf');

const bot = new Telegraf(process.env.TOKEN_API);

bot.start((ctx)=>{
    console.log(ctx.from);
    ctx.reply(`Hola ${ctx.from.first_name}!`)
})

bot.help((ctx)=>{
    ctx.reply('Ayuda!!')
})

bot.settings((ctx)=>{
    ctx.reply('Settings')
})

bot.command('mycommand', (ctx)=>{
    ctx.reply('My custom command!')
})

bot.hears('cat', (ctx)=>{
    ctx.reply('Tu gato es asombroso!')
})

bot.hears(/clima*/i , async (ctx)=>{
    console.log(`Mensaje enviado: ${ctx.update.message.text}`);
    ciudad = (ctx.update.message.text).split(" ")[1];
    console.log(`Ciudad: ${ciudad}`);
    ctx.reply(await getInfo(ciudad))
})

bot.on('sticker', (ctx)=>{
    ctx.reply('Me gustan los stickers')
})

bot.launch()

const getInfo = async(direccion) => {
    try {
        const coords = await lugar.getLugarLatLng(direccion);
        const temp = await clima.getClima(coords.lat, coords.lng);
        response = `El clima de ${coords.direccion} es de ${temp}°C.`
        console.log('Response: '+ response); 
        return response;
    } catch (e) {
        console.log('Error: '+ e); 
        return `No se pudo determinar el clima de ${direccion}`;
    }
}

exports.bot = bot;