require('./config/config')
require('./bot/bot');

const express = require('express');

console.log(`${process.env.NODE_ENV} environment`);

app = express();

// TODO: renderizar un html
app.get('/', (req, res) => {
    res.send('Takechi Bot');
});

app.listen(process.env.PORT, () => {
    console.log(`Server running on port ${process.env.PORT}`);
});