require('dotenv').config()
// ==================
// Puerto
// ==================
process.env.PORT = process.env.PORT || 3000



// ==================
// Entorno
// ==================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';


// ==================
// Token
// ==================
process.env.TOKEN = process.env.TOKEN_API || '';